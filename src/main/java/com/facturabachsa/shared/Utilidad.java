/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.shared;

import com.facturabachsa.resource.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;

/**
 *
 * @author Alberto Fernandez
 */
public class Utilidad {

    public static String getText(String valor) {

        if (valor == null) {
            return "";
        } else {
            valor = valor.replace("'", "''");
        }

        return valor;
    }

    public static String getQueryDocCabOrigenTxt(String fechaIni, String fechaFin) throws IOException, URISyntaxException {
        InputStream is = Resource.getInstance().getQueryDocCabOrigenTxt();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        byte[] fileContent = buffer.toByteArray();
        String query = new String(fileContent, Constans.UTF_8);
        query = query.replaceAll("@fecha_inicio@", fechaIni);
        query = query.replaceAll("@fecha_fin@", fechaFin);        
        return query;
    }

}
