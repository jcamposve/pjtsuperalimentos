/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.shared;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
public class FEtxt {    
    private final String nameFile;    

    public FEtxt(String nameFile) {       
        this.nameFile = nameFile;
    }

    public void exportarTxt(String txt) throws Exception {
        try {                        
            this.setDataTxt(txt, nameFile);
        } catch (Exception e) {
            throw e;
        }
    }

    private void setDataTxt(String txt, String nameFile) throws Exception {
        BufferedWriter w = null;
        try {
            this.validateDirectory();
            File filetxt = new File(Constans.PATH_DIRECT_TXT + nameFile + Constans.FORMAT_TXT);
            w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filetxt), "Cp1252"));
            w.append(txt.replaceAll("(?m)^\\s", ""));
            //w.append("\r\n");
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } finally {
            if (w != null) {
                w.close();
            }
        }
    }

    private void validateDirectory() {
        File theDir = new File(Constans.PATH_DIRECT_TXT);
        if (theDir.exists()) {
            return;
        }
        theDir.mkdir();
    }

}

