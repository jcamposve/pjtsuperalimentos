/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.dao;

import com.facturabachsa.bean.DocumentoCabecera;
import com.facturabachsa.bean.DocumentoDetalle;
import com.facturabachsa.shared.ExceptionHandler;
import com.facturabachsa.shared.FEtxt;
import com.facturabachsa.shared.Utilidad;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alberto Fernandez
 */
public class DaoDocumentoCabecera {

    static Connection conSql;
    static Connection conHsql;

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    public DaoDocumentoCabecera() {

    }

    public HashMap<String, DocumentoCabecera> GetCabeceraOrigen(String fechaIni, String fechaFin) throws SQLException {
        HashMap<String, DocumentoCabecera> dataDocCab = new HashMap();
        ResultSet resultSet = null;
        Statement statement = null;
        logger.info("Procesando Desde " + fechaIni + " hasta " + fechaFin);
        try {
            String sql = Utilidad.getQueryDocCabOrigenTxt(fechaIni, fechaFin);
            //logger.debug("Query: " + sql);
            conSql = SingleSqlServer.getSqlServer().getCnx();
            statement = conSql.createStatement();
            resultSet = statement.executeQuery(sql);

            HashMap<String, DocumentoCabecera> listaDocCabDest;
            DaoDocumentoCabecera ddc = new DaoDocumentoCabecera();
            listaDocCabDest = ddc.GetCabeceraDestino(fechaIni, fechaFin);
            String idCabecera = "";
            while (resultSet.next()) {
                idCabecera = resultSet.getString(3) + resultSet.getString(4) + resultSet.getString(5);
                try {
                    String resultado11 = resultSet.getString(11).replace("'", "''");
                    String resultado13 = resultSet.getString(13).replace("'", "''");
                    String resultado17 = resultSet.getString(17).replace("'", "''");
                    String resultado18 = resultSet.getString(18) == null ? resultSet.getString(18) : resultSet.getString(18).replace("'", "''").replaceAll("\\r\\n|\\r|\\n", " ").replaceAll("\\r\\n|\\r|\\n", " ");

                    DocumentoCabecera docCab = new DocumentoCabecera();
                    //logger.info("ID_CABECERA ORIGEN: " + resultSet.getString(3) + resultSet.getString(4) + resultSet.getString(5));
                    docCab.setLlaveCabecera(resultSet.getString(3) + resultSet.getString(4) + resultSet.getString(5));
                    docCab.setCodiEmp(resultSet.getString(1));
                    docCab.setFormaPago(resultSet.getString(2));
                    docCab.setTipoDte(resultSet.getString(3));
                    docCab.setSerie(resultSet.getString(4));
                    docCab.setCorrelativo(resultSet.getString(5));
                    docCab.setFchEmis(resultSet.getString(6));
                    docCab.setHoraEmision(resultSet.getString(7));
                    docCab.setMoneda(resultSet.getString(8));
                    docCab.setRutEmis(resultSet.getString(9));
                    docCab.setTipoRucEmis(resultSet.getString(10));
                    docCab.setRznSocEmis(resultado11.replaceAll("\\r\\n|\\r|\\n", " "));
                    docCab.setComuEmis(resultSet.getString(12));
                    docCab.setDirEmis(resultado13.replaceAll("\\r\\n|\\r|\\n", " "));
                    docCab.setCodigoLocalAnexo(resultSet.getString(14));
                    docCab.setTipoRutReceptor(resultSet.getString(15).trim());
                    docCab.setRutRecep(resultSet.getString(16).trim());
                    docCab.setRznSocRecep(resultado17.replaceAll("\\r\\n|\\r|\\n", " "));
                    docCab.setDirRecep(resultado18);
                    docCab.setSustento(resultSet.getString(19));
                    docCab.setTipoNotaCredito(resultSet.getString(20));
                    docCab.setTipoOperacion(resultSet.getString(21));
                    docCab.setFactorCargoDescuento(resultSet.getString(22));
                    docCab.setTpoDocRef(resultSet.getString(23));
                    docCab.setSerieRef(resultSet.getString(24));
                    docCab.setFolioRef(resultSet.getString(25));
                    docCab.setFechVencFact(resultSet.getString(26));
                    docCab.setFechaRef(resultSet.getString(27));
                    docCab.setOrdenCompra(resultSet.getString(28));
                    docCab.setGuia(resultSet.getString(29));
                    docCab.setTipoCambio(resultSet.getString(30));
                    docCab.setMntNeto(resultSet.getString(31));
                    docCab.setMntExe(resultSet.getString(32));
                    docCab.setMntTotGrat(resultSet.getString(33));
                    docCab.setMntTotAnticipo(resultSet.getString(34));
                    docCab.setMntTotal(resultSet.getString(35));
                    docCab.setCodigoImpuesto(resultSet.getString(36));
                    docCab.setMontoImpuesto(resultSet.getString(37));
                    docCab.setTasaImpuesto(resultSet.getString(38));
                    if (!listaDocCabDest.containsKey(docCab.getLlaveCabecera())) {
                        dataDocCab.put(docCab.getLlaveCabecera(), docCab);
                    }
                } catch (Exception ex) {
                    System.out.println("Error Origen Cabecera: " + idCabecera + ": \n" + ex);
                    logger.error("Error Origen Cabecera: " + idCabecera);
                    ExceptionHandler.handleException(ex, logger);
                }
            }

        } catch (Exception ex) {
            System.out.println("Error: " + ex);
            ExceptionHandler.handleException(ex, logger);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
        }

        return dataDocCab;
    }

    public HashMap<String, DocumentoCabecera> GetCabeceraDestino(String fechaDesIni, String fechaDesFin) throws SQLException {
        HashMap<String, DocumentoCabecera> dataDocCab = new HashMap();
        ResultSet resultSet = null;
        CallableStatement statement = null;

        try {
            conHsql = SingleHyperSql.getHyperSql().getCnx();

            statement = conHsql.prepareCall("call spObtenerDocumentoCabecera(?,?)");
            statement.setString(1, fechaDesIni);
            statement.setString(2, fechaDesFin);
            boolean isResult = statement.execute();
            isResult = statement.getMoreResults();
            resultSet = statement.getResultSet();
            String idCabecera = "";
            while (resultSet.next()) {
                idCabecera = resultSet.getString(3) + resultSet.getString(4) + resultSet.getString(5);
                try {
                    String resultado11 = resultSet.getString(11).replace("'", "''");
                    String resultado13 = resultSet.getString(13).replace("'", "''");
                    String resultado17 = resultSet.getString(17).replace("'", "''");
                    String resultado18 = resultSet.getString(18) == null ? resultSet.getString(18) : resultSet.getString(18).replace("'", "''").replaceAll("\\r\\n|\\r|\\n", " ");

                    DocumentoCabecera docCab = new DocumentoCabecera();

                    docCab.setLlaveCabecera(resultSet.getString(3) + resultSet.getString(4) + resultSet.getString(5));
                    docCab.setCodiEmp(resultSet.getString(1));
                    docCab.setFormaPago(resultSet.getString(2));
                    docCab.setTipoDte(resultSet.getString(3));
                    docCab.setSerie(resultSet.getString(4));
                    docCab.setCorrelativo(resultSet.getString(5));
                    docCab.setFchEmis(resultSet.getString(6));
                    docCab.setHoraEmision(resultSet.getString(7));
                    docCab.setMoneda(resultSet.getString(8));
                    docCab.setRutEmis(resultSet.getString(9));
                    docCab.setTipoRucEmis(resultSet.getString(10));
                    docCab.setRznSocEmis(resultado11.replaceAll("\\r\\n|\\r|\\n", " "));
                    docCab.setComuEmis(resultSet.getString(12));
                    docCab.setDirEmis(resultado13.replaceAll("\\r\\n|\\r|\\n", " "));
                    docCab.setCodigoLocalAnexo(resultSet.getString(14));
                    docCab.setTipoRutReceptor(resultSet.getString(15).trim());
                    docCab.setRutRecep(resultSet.getString(16).trim());
                    docCab.setRznSocRecep(resultado17.replaceAll("\\r\\n|\\r|\\n", " "));
                    docCab.setDirRecep(resultado18);
                    docCab.setSustento(resultSet.getString(19));
                    docCab.setTipoNotaCredito(resultSet.getString(20));
                    docCab.setMntNeto(resultSet.getString(21));
                    docCab.setMntExe(resultSet.getString(22));
                    docCab.setMntTotGrat(resultSet.getString(23));
                    docCab.setMntTotAnticipo(resultSet.getString(24));
                    docCab.setMntTotal(resultSet.getString(25));
                    docCab.setTipoOperacion(resultSet.getString(26));
                    docCab.setFechVencFact(resultSet.getString(27));
                    docCab.setCodigoImpuesto(resultSet.getString(28));
                    docCab.setMontoImpuesto(resultSet.getString(29));
                    docCab.setTasaImpuesto(resultSet.getString(30));

                    dataDocCab.put(docCab.getLlaveCabecera(), docCab);
                } catch (Exception ex) {
                    System.out.println("Error Destino Cabecera: " + idCabecera + ": \n" + ex);
                    logger.error("Error Destino Cabecera: " + idCabecera);
                    ExceptionHandler.handleException(ex, logger);
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            ExceptionHandler.handleException(ex, logger);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
        }

        return dataDocCab;
    }

    public int InsertarDocumento(String fechaIni, String fechaFin) throws SQLException {
        int result = 0;
        HashMap<String, DocumentoCabecera> listaDocCabOrig;

        try {

            listaDocCabOrig = GetCabeceraOrigen(fechaIni, fechaFin);
            conHsql = SingleHyperSql.getHyperSql().getCnx();

            if (!listaDocCabOrig.isEmpty()) {
                listaDocCabOrig.entrySet().forEach((Map.Entry<String, DocumentoCabecera> entry) -> {
                    String docCab;
                    docCab = "INSERT INTO DOC_CAB"
                            + " values('"
                            + entry.getValue().getCodiEmp() + "', '"
                            + entry.getValue().getFormaPago() + "', '"
                            + entry.getValue().getTipoDte() + "', '"
                            + entry.getValue().getSerie() + "', '"
                            + entry.getValue().getCorrelativo() + "', '"
                            + entry.getValue().getFchEmis() + "', '"
                            + entry.getValue().getHoraEmision() + "', '"
                            + entry.getValue().getMoneda() + "', '"
                            + entry.getValue().getRutEmis() + "', '"
                            + entry.getValue().getTipoRucEmis() + "', '"
                            + entry.getValue().getRznSocEmis() + "', '"
                            + entry.getValue().getComuEmis() + "', '"
                            + entry.getValue().getDirEmis() + "', '"
                            + entry.getValue().getCodigoLocalAnexo() + "', '"
                            + entry.getValue().getTipoRutReceptor().trim() + "', '"
                            + entry.getValue().getRutRecep().trim() + "', '"
                            + entry.getValue().getRznSocRecep() + "', '"
                            + entry.getValue().getDirRecep() + "', '"
                            + entry.getValue().getSustento() + "', '"
                            + entry.getValue().getTipoNotaCredito() + "', '"
                            + entry.getValue().getMntNeto() + "', '"
                            + entry.getValue().getMntExe() + "', '"
                            + entry.getValue().getMntTotGrat() + "', '"
                            + entry.getValue().getMntTotAnticipo() + "', '"
                            + entry.getValue().getMntTotal() + "', '"
                            + entry.getValue().getTipoOperacion() + "', '"
                            + entry.getValue().getFechVencFact() + "', '"
                            + entry.getValue().getCodigoImpuesto() + "', '"
                            + entry.getValue().getMontoImpuesto() + "', '"
                            + entry.getValue().getTasaImpuesto() + "', '"
                            + entry.getValue().getFactorCargoDescuento() + "', '"
                            + entry.getValue().getTpoDocRef() + "', '"
                            + entry.getValue().getSerieRef() + "', '"
                            + entry.getValue().getFolioRef() + "', '"
                            + entry.getValue().getOrdenCompra() + "', '"
                            + entry.getValue().getGuia() + "', '"
                            + entry.getValue().getFechaRef() + "', '"
                            + entry.getValue().getTipoCambio()+ "');";

                    try {
                        //logger.debug(docCab);
                        conHsql.createStatement()
                                .executeUpdate(docCab);

                        String tipoDte = entry.getValue().getTipoDte();
                        String serie = entry.getValue().getSerie();
                        String correlativo = entry.getValue().getCorrelativo();

                        HashMap<String, DocumentoDetalle> listaDetalle;
                        DaoDocumentoDetalle daoDocDet = new DaoDocumentoDetalle();
                        listaDetalle = daoDocDet.GetDetalleOrigen(tipoDte, serie, correlativo);

                        if (!listaDetalle.isEmpty()) {
                            daoDocDet.InsertarDetalle(listaDetalle);
                        }

                        String valor = GetFactura(conHsql, tipoDte, serie, correlativo);
                        FEtxt fe = new FEtxt(entry.getValue().getRutEmis() + "-" + entry.getValue().getTipoDte() + "-" + entry.getValue().getSerie() + "-" + entry.getValue().getCorrelativo());
                        fe.exportarTxt(valor);

                    } catch (Exception ex) {
                        Logger.getLogger(DaoDocumentoCabecera.class.getName()).log(Level.SEVERE, null, ex);
                        ExceptionHandler.handleException(ex, logger);
                    }
                });
                result = 1;
            } else {
                result = 0;
            }
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            result = 2;
        } finally {
        }
        return result;
    }

    public String GetFactura(Connection conHsql, String tipoDoc, String serie, String correlativo) throws SQLException {
        String factura = null;

        try (CallableStatement callCab = conHsql.prepareCall("CALL spObtenerFactura(?,?,?,?)")) {
            callCab.setString(1, tipoDoc);
            callCab.setString(2, serie);
            callCab.setString(3, correlativo);
            callCab.setString(4, factura);
            callCab.execute();
            factura = callCab.getString(4);
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            ExceptionHandler.handleException(ex, logger);
        }
        return factura;
    }

}
