/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.dao;


import com.facturabachsa.bean.ParamFactura;
import com.facturabachsa.shared.ExceptionHandler;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alberto Fernandez
 */
public class DaoParamIni {
    
    static Connection conHsql;
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());
    
     public void GetParamFacturacion(ParamFactura paramfac) throws SQLException {
        CallableStatement callCab=null;
        try {
            conHsql = SingleHyperSql.getHyperSql().getCnx();
            callCab= conHsql.prepareCall("CALL spObtenerParametrosFactura(?,?,?)");
            callCab.execute();
            
            paramfac.setFechaIni(callCab.getString(1));
            paramfac.setFechaFin(callCab.getString(2));
            paramfac.setCantDias(callCab.getString(3));
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            ExceptionHandler.handleException(ex, logger);
        } finally {
            if(callCab!=null){
                callCab.close();
            }
        }
       
    }
}
