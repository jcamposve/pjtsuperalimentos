/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.dao;

import com.facturabachsa.bean.DocumentoDetalle;
import com.facturabachsa.shared.ExceptionHandler;
import com.facturabachsa.shared.Utilidad;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alberto Fernandez
 */
public class DaoDocumentoDetalle {
    
    static Connection conSql;
    static Connection conHsql;
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());
    
    public HashMap<String, DocumentoDetalle> GetDetalleOrigen(String tipoDTE,String serie, String correlativo) throws SQLException {
        HashMap<String, DocumentoDetalle> dataDetalle = new HashMap();
        ResultSet resultSet = null ;
        Statement statement = null ;

        try {
            String sql = "Declare @tipoDo VARCHAR(100); \n" +
                        "SET @tipoDo = '" + tipoDTE + "'\n" +
                        " select  \n" +
                        " ROW_NUMBER() OVER(ORDER BY mi.prefijo ASC) AS NroLinDet,\n" +
                        " case \n" +
                        " when cb.comprobante ='Boleta de Venta' then '03'\n" +
                        " when cb.comprobante ='Factura' then '01' \n" +
                        " when cb.comprobante ='Nota de crédito' then '07' \n" +
                        " when cb.comprobante ='Nota de débito' then '08'\n" +
                        " end as TipoDTE,\n" +
                        " TI.clasificacion,\n" +
                        " CB.prefijo as Serie,\n" +
                        " CB.numero_de_documento as Correlativo,\n" +
                        " case\n" +
                        " when TI.clasificacion='Producto' then abs(MI.Cant) \n" +
                        " when TI.clasificacion='Servicio' then abs(1.00)\n" +
                        " when MI.Descripción = 'ANTICIPO RECIBIDO' then abs(1.00)  \n" +
                        "else 1              \n" +
                        " end\n" +
                        " as QtyItem,    \n" +
                        " case\n" +
                        " when MI.Descripción = 'ANTICIPO RECIBIDO' then 'NIU' \n" +
                        " else TI.Unidad_de_Medida \n" +
                        " end\n" +
                        " as UnmdItem,\n" +
                        " MI.CódigoInventario as VlrCodigo,\n" +
                        " MI.Descripción as NmbItem,\n" +
                        " case\n" +
                        "when MI.moneda is null then cast((abs(MI.valor_unitario) + abs(MI.valor_unitario* MI.iva ))as numeric(18,10))\n" +
                        " when MI.moneda = 'Dolares'  then CAST(abs(MI.valor_unitario_otra_moneda)  + abs(MI.valor_unitario_otra_moneda* MI.iva) AS NUMERIC(18,10))\n" +
                        "end as  PrcItem,\n" +
                        "case\n" +
                        "when MI.descuento_porcentaje='100' or CB.Forma_de_pago='1027' then 0.00\n" +
                        " when MI.moneda is null then cast(abs(MI.valor_unitario)as numeric(18,2))\n" +
                        "when MI.moneda = 'Dolares'  then CAST(abs(MI.valor_unitario_otra_moneda)  AS NUMERIC(18,2))\n" +
                        " end as PrcItemSinIgv,\n" +
                        " case\n" +
                        " when MI.moneda is null then CAST(abs(MI.subtotal) AS NUMERIC (12,2) )\n" +
                        " when MI.moneda = 'Dolares'  then CAST(abs(MI.subtotal_otra_moneda) AS NUMERIC (12,2))\n" +
                        " end as MontoItem,\n" +
                        " case\n" +
                        " when Mi.iva=0.18 and MI.descuento_porcentaje=100 then '15'\n" +
                        " when Mi.iva=0.18 and CB.Forma_de_pago='1027' then '13'\n" +
                        " when Mi.iva=0.00 and CB.Forma_de_pago='1027' then '32'\n" +
                        " when Mi.iva=0.00 and MI.descuento_porcentaje=100 then '31'\n" +
                        " when MI.iva=0 then '30'\n" +
                        " else '10'\n" +
                        " end as IndExe,\n" +
                        " case\n" +
                        " when MI.descuento_porcentaje='100' or CB.Forma_de_pago='1027' then '9996'\n" +
                        " when MI.iva=0 then '9998'\n" +
                        " else '1000'\n" +
                        " end as CodigoTipoIgv,\n" +
                        "CASE\n" +
                        "when MI.descuento_porcentaje='100' or CB.Forma_de_pago='1027' then 18.00\n" +
                        "ELSE\n" +
                        " CAST(MI.iva*100 as numeric (5,2))\n" +
                        "END  as TasaIgv,\n" +
                        " case\n" +
                        " when MI.moneda is null then CAST(abs(MI.SUBTOTAL*MI.IVA)  as numeric (12,2))\n" +
                        " when MI.moneda = 'Dolares'  then cast( abs(MI.SUBTOTAL_otra_moneda*MI.IVA)as numeric (12,2)) \n" +
                        " end as ImpuestoIgv,\n" +
                        " TI.personalizado1 AS CodigoProductoSunat\n" +
                        " from dbo.Vista_Auxiliar_Movimientos_Inventario MI \n" +
                        " inner join dbo.Vista_Tabla_Inventarios TI on\n" +
                        " MI.códigoinventario=TI.codigo_producto inner join\n" +
                        "  (select distinct cb.numero_de_documento,\n" +
                        "                   cb.prefijo,\n" +
                        " 				  cb.tipo_de_documento,\n" +
                        " 				  cb.comprobante,\n" +
                        " 				  cb.Anulado,\n" +
                        " 				  cb.forma_de_pago\n" +
                        "     from dbo.Vista_Tabla_Encabezados cb \n" +
                        "    where cb.comprobante = case \n" +
                        " 		when @tipoDo = '03' then 'Boleta de Venta'\n" +
                        " 		when @tipoDo = '01' then 'Factura'\n" +
                        " 		when @tipoDo = '07' then 'Nota de crédito'\n" +
                        " 		when @tipoDo = '08' then 'Nota de débito' end\n" +
                        "      and cb.prefijo='" + serie + "'\n" +
                        " 	 and cb.numero_de_documento='" + correlativo + "') CB on\n" +
                        " MI.prefijo = cb.prefijo and Mi.numero_documento = CB.numero_de_documento\n" +
                        " where cb.tipo_de_documento in ('FV','NDCL','DMC','NCCL')  AND \n" +
                        " cb.comprobante  in ('Boleta de Venta','Factura','Nota de crédito','Nota de débito') and\n" +
                        " cb.anulado=0 and mi.tipo_documento not in ('DREM') and \n" +
                        "len(cb.prefijo)=4";
            logger.info("Query: "+sql);
            conSql = SingleSqlServer.getSqlServer().getCnx();
            statement = conSql.createStatement();
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                DocumentoDetalle docDet = new DocumentoDetalle();

                docDet.setLlaveDetalle(Utilidad.getText(resultSet.getString(2)) + 
                                       Utilidad.getText(resultSet.getString(3)) + 
                                       Utilidad.getText(resultSet.getString(4)) + 
                                       Utilidad.getText(resultSet.getString(1)));
                
                docDet.setNroLinDet(Utilidad.getText(resultSet.getString(1)));
                docDet.setTipoDTE(Utilidad.getText(resultSet.getString(2)));
                docDet.setClasificacion(Utilidad.getText(resultSet.getString(3)));
                docDet.setSerie(Utilidad.getText(resultSet.getString(4)));
                docDet.setCorrelativo(Utilidad.getText(resultSet.getString(5)));
                docDet.setQtyItem(Utilidad.getText(resultSet.getString(6)));
                docDet.setUnmdItem(Utilidad.getText(resultSet.getString(7)));
                docDet.setVlrCodigo(Utilidad.getText(resultSet.getString(8)));
                docDet.setNmbItem(Utilidad.getText(resultSet.getString(9)));
                docDet.setPrcItem(Utilidad.getText(resultSet.getString(10)));
                docDet.setPrcItemSinIgv(Utilidad.getText(resultSet.getString(11)));
                docDet.setMontoItem(Utilidad.getText(resultSet.getString(12)));
                docDet.setIndExe(Utilidad.getText(resultSet.getString(13)));
                docDet.setCodigoTipoIgv(Utilidad.getText(resultSet.getString(14)));
                docDet.setTasaIgv(Utilidad.getText(resultSet.getString(15)));
                docDet.setImpuestoIgv(Utilidad.getText(resultSet.getString(16)));
                docDet.setCodigoProductoSunat(Utilidad.getText(resultSet.getString(17)));

                dataDetalle.put(docDet.getLlaveDetalle(), docDet);
            }

        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getLocalizedMessage());
            ExceptionHandler.handleException(ex, logger);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
        }

        return dataDetalle;
    }
    
    public int InsertarDetalle(HashMap<String, DocumentoDetalle> listaDetalle) throws SQLException {
        int result = 0; 
        try {
            
            conHsql = SingleHyperSql.getHyperSql().getCnx();

            if (!listaDetalle.isEmpty()) {
                listaDetalle.entrySet().forEach((Map.Entry<String, DocumentoDetalle> entry) -> {
                    String docDet;
                    docDet = "INSERT INTO DOC_DET"
                            + " values('"
                            + entry.getValue().getNroLinDet() + "', '"
                            + entry.getValue().getTipoDTE() + "', '"
                            + entry.getValue().getSerie() + "', '"
                            + entry.getValue().getCorrelativo() + "', '"
                            + entry.getValue().getClasificacion() + "', '"
                            + entry.getValue().getQtyItem() + "', '"
                            + entry.getValue().getUnmdItem() + "', '"
                            + entry.getValue().getVlrCodigo() + "', '"
                            + entry.getValue().getNmbItem() + "', '"
                            + entry.getValue().getPrcItem() + "', '"
                            + entry.getValue().getPrcItemSinIgv() + "', '"
                            + entry.getValue().getMontoItem() + "', '"
                            + entry.getValue().getIndExe() + "', '"
                            + entry.getValue().getCodigoTipoIgv() + "', '"
                            + entry.getValue().getTasaIgv() + "', '"
                            + entry.getValue().getImpuestoIgv() + "', '"
                            + entry.getValue().getCodigoProductoSunat() + "');";
                    try {
                        conHsql.createStatement()
                                .executeUpdate(docDet);
                        
                    } catch (SQLException ex) {
                        Logger.getLogger(DaoDocumentoCabecera.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                result = 1;
            }else{
                result = 0;
            }            
        } catch (Exception ex) {
            System.out.println("Error: " + ex);
            result = 2;
            ExceptionHandler.handleException(ex, logger);
        } finally {
        }
        return result;
    }
    

    
}
