/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.dao;

import com.facturabachsa.shared.ExceptionHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
/**
 *
 * @author root
 */
@Component
public class SingleSqlServer {
    private static SingleSqlServer sqlServer;
    private Connection cnx;
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    public SingleSqlServer(){
        try{
        //String url = "jdbc:sqlserver://localhost:1433;databaseName=SUPERALIMENTOS DEL PERU SOCIEDAD ANONIMA CERRADA";
        //String user = "sa";
        //String password = "sa_123";
        String url = "jdbc:sqlserver://SERVIDOR\\WORLDOFFICE;databaseName=SUPERALIMENTOS DEL PERU SOCIEDAD ANONIMA CERRADA";  
        String user = "wo_cliente";              
        String password = "wo_cliente";
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        Class.forName(driver).newInstance();
        cnx = DriverManager.getConnection(url, user, password);
        //cnx.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        //cnx.setAutoCommit(false);        
        System.out.println("creando conexion SQL Server");
        }catch(ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex){
            System.out.println(ex.getMessage());
            ExceptionHandler.handleException(ex, logger);
        }
    }

    public static SingleSqlServer getSqlServer() {
        if(sqlServer==null){
            sqlServer = new SingleSqlServer();
        }
        return sqlServer;
    }

    public Connection getCnx() {
        return cnx;
    }

}
