/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.dao;




import com.facturabachsa.shared.ExceptionHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



/**
 *
 * @author root
 */
@Component
public class SingleHyperSql {
    private static SingleHyperSql hyperSql;
    private Connection cnx;
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());
    
    private SingleHyperSql() {        
        try{            
            
        String url = "jdbc:hsqldb:hsql://localhost/superalimentosdb;hsqldb.lock_file=false;";
        String user = "SA"; 
        String password = "sa_123$$";
        String driver = "org.hsqldb.jdbc.JDBCDriver";
        Class.forName(driver).newInstance();
        cnx = DriverManager.getConnection(url, user, password);            
            System.out.println("creando conexion HyperSQL");
        }catch(ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex){
            System.out.println(ex.getMessage());
            ExceptionHandler.handleException(ex, logger);
        }
    }

    public static SingleHyperSql getHyperSql() {
        if(hyperSql==null){
            hyperSql = new SingleHyperSql();
        }
        return hyperSql;
    }

    public Connection getCnx() {
        return cnx;
    }
        
}
