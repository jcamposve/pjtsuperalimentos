/*
 * Decompiled with CFR 0_133.
 * 
 * Could not load the following classes:
 *  com.facturabachsa.bean.DocumentoCabecera
 */
package com.facturabachsa.bean;

import java.io.Serializable;

public class DocumentoCabecera
implements Serializable {
    private String llaveCabecera;
    private String codiEmp;
    private String formaPago;
    private String tipoDte;
    private String serie;
    private String correlativo;
    private String fchEmis;
    private String horaEmision;
    private String moneda;
    private String rutEmis;
    private String tipoRucEmis;
    private String rznSocEmis;
    private String comuEmis;
    private String dirEmis;
    private String codigoLocalAnexo;
    private String tipoRutReceptor;
    private String rutRecep;
    private String rznSocRecep;
    private String dirRecep;
    private String sustento;
    private String tipoNotaCredito;
    private String mntNeto;
    private String mntExe;
    private String mntTotGrat;
    private String mntTotAnticipo;
    private String mntTotal;
    private String tipoOperacion;
    private String fechVencFact;
    private String codigoImpuesto;
    private String montoImpuesto;
    private String tasaImpuesto;
    private String factorCargoDescuento;
    private String tpoDocRef;
    private String serieRef;
    private String folioRef;
    private String FechaRef;
    private String OrdenCompra;
    private String Guia;
    private String TipoCambio;

    public String getTipoCambio() {
        return this.TipoCambio;
    }

    public void setTipoCambio(String TipoCambio) {
        this.TipoCambio = TipoCambio;
    }

    public String getFechaRef() {
        return this.FechaRef;
    }

    public void setFechaRef(String FechaRef) {
        this.FechaRef = FechaRef;
    }

    public String getOrdenCompra() {
        return this.OrdenCompra;
    }

    public void setOrdenCompra(String OrdenCompra) {
        this.OrdenCompra = OrdenCompra;
    }

    public String getGuia() {
        return this.Guia;
    }

    public void setGuia(String Guia) {
        this.Guia = Guia;
    }

    public String getLlaveCabecera() {
        return this.llaveCabecera;
    }

    public void setLlaveCabecera(String llaveCabecera) {
        this.llaveCabecera = llaveCabecera;
    }

    public String getCodiEmp() {
        return this.codiEmp;
    }

    public void setCodiEmp(String codiEmp) {
        this.codiEmp = codiEmp;
    }

    public String getFormaPago() {
        return this.formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getTipoDte() {
        return this.tipoDte;
    }

    public void setTipoDte(String tipoDte) {
        this.tipoDte = tipoDte;
    }

    public String getSerie() {
        return this.serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getCorrelativo() {
        return this.correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public String getFchEmis() {
        return this.fchEmis;
    }

    public void setFchEmis(String fchEmis) {
        this.fchEmis = fchEmis;
    }

    public String getHoraEmision() {
        return this.horaEmision;
    }

    public void setHoraEmision(String horaEmision) {
        this.horaEmision = horaEmision;
    }

    public String getMoneda() {
        return this.moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getRutEmis() {
        return this.rutEmis;
    }

    public void setRutEmis(String rutEmis) {
        this.rutEmis = rutEmis;
    }

    public String getTipoRucEmis() {
        return this.tipoRucEmis;
    }

    public void setTipoRucEmis(String tipoRucEmis) {
        this.tipoRucEmis = tipoRucEmis;
    }

    public String getRznSocEmis() {
        return this.rznSocEmis;
    }

    public void setRznSocEmis(String rznSocEmis) {
        this.rznSocEmis = rznSocEmis;
    }

    public String getComuEmis() {
        return this.comuEmis;
    }

    public void setComuEmis(String comuEmis) {
        this.comuEmis = comuEmis;
    }

    public String getDirEmis() {
        return this.dirEmis;
    }

    public void setDirEmis(String dirEmis) {
        this.dirEmis = dirEmis;
    }

    public String getCodigoLocalAnexo() {
        return this.codigoLocalAnexo;
    }

    public void setCodigoLocalAnexo(String codigoLocalAnexo) {
        this.codigoLocalAnexo = codigoLocalAnexo;
    }

    public String getTipoRutReceptor() {
        return this.tipoRutReceptor;
    }

    public void setTipoRutReceptor(String tipoRutReceptor) {
        this.tipoRutReceptor = tipoRutReceptor;
    }

    public String getRutRecep() {
        return this.rutRecep;
    }

    public void setRutRecep(String rutRecep) {
        this.rutRecep = rutRecep;
    }

    public String getRznSocRecep() {
        return this.rznSocRecep;
    }

    public void setRznSocRecep(String rznSocRecep) {
        this.rznSocRecep = rznSocRecep;
    }

    public String getDirRecep() {
        return this.dirRecep;
    }

    public void setDirRecep(String dirRecep) {
        this.dirRecep = dirRecep;
    }

    public String getSustento() {
        return this.sustento;
    }

    public void setSustento(String sustento) {
        this.sustento = sustento;
    }

    public String getTipoNotaCredito() {
        return this.tipoNotaCredito;
    }

    public void setTipoNotaCredito(String tipoNotaCredito) {
        this.tipoNotaCredito = tipoNotaCredito;
    }

    public String getMntNeto() {
        return this.mntNeto;
    }

    public void setMntNeto(String mntNeto) {
        this.mntNeto = mntNeto;
    }

    public String getMntExe() {
        return this.mntExe;
    }

    public void setMntExe(String mntExe) {
        this.mntExe = mntExe;
    }

    public String getMntTotGrat() {
        return this.mntTotGrat;
    }

    public void setMntTotGrat(String mntTotGrat) {
        this.mntTotGrat = mntTotGrat;
    }

    public String getMntTotAnticipo() {
        return this.mntTotAnticipo;
    }

    public void setMntTotAnticipo(String mntTotAnticipo) {
        this.mntTotAnticipo = mntTotAnticipo;
    }

    public String getMntTotal() {
        return this.mntTotal;
    }

    public void setMntTotal(String mntTotal) {
        this.mntTotal = mntTotal;
    }

    public String getTipoOperacion() {
        return this.tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getFechVencFact() {
        return this.fechVencFact;
    }

    public void setFechVencFact(String fechVencFact) {
        this.fechVencFact = fechVencFact;
    }

    public String getCodigoImpuesto() {
        return this.codigoImpuesto;
    }

    public void setCodigoImpuesto(String codigoImpuesto) {
        this.codigoImpuesto = codigoImpuesto;
    }

    public String getMontoImpuesto() {
        return this.montoImpuesto;
    }

    public void setMontoImpuesto(String montoImpuesto) {
        this.montoImpuesto = montoImpuesto;
    }

    public String getTasaImpuesto() {
        return this.tasaImpuesto;
    }

    public void setTasaImpuesto(String tasaImpuesto) {
        this.tasaImpuesto = tasaImpuesto;
    }

    public String getFactorCargoDescuento() {
        return this.factorCargoDescuento;
    }

    public void setFactorCargoDescuento(String factorCargoDescuento) {
        this.factorCargoDescuento = factorCargoDescuento;
    }

    public String getTpoDocRef() {
        return this.tpoDocRef;
    }

    public void setTpoDocRef(String tpoDocRef) {
        this.tpoDocRef = tpoDocRef;
    }

    public String getSerieRef() {
        return this.serieRef;
    }

    public void setSerieRef(String serieRef) {
        this.serieRef = serieRef;
    }

    public String getFolioRef() {
        return this.folioRef;
    }

    public void setFolioRef(String folioRef) {
        this.folioRef = folioRef;
    }
}
