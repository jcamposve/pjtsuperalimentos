/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.bean;

import java.io.Serializable;

/**
 *
 * @author Alberto Fernandez
 */
public class ParamFactura implements Serializable {
    private String fechaIni;
    private String cantDias;
    private String fechaFin;

    public String getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(String fechaIni) {
        this.fechaIni = fechaIni;
    }

    public String getCantDias() {
        return cantDias;
    }

    public void setCantDias(String cantDias) {
        this.cantDias = cantDias;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }
    
    
    
}
