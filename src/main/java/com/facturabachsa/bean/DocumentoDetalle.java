/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.bean;

import java.io.Serializable;

/**
 *
 * @author Alberto Fernandez
 */
public class DocumentoDetalle implements Serializable  {
    private String llaveDetalle;
    private String nroLinDet;    
    private String tipoDTE; 
    private String serie;
    private String correlativo;
    private String clasificacion;
    private String qtyItem;
    private String unmdItem;
    private String vlrCodigo;
    private String nmbItem;
    private String prcItem;
    private String prcItemSinIgv;
    private String montoItem;
    private String indExe;
    private String codigoTipoIgv;
    private String tasaIgv;
    private String impuestoIgv;
    private String codigoProductoSunat;

    public String getLlaveDetalle() {
        return llaveDetalle;
    }

    public void setLlaveDetalle(String llaveDetalle) {
        this.llaveDetalle = llaveDetalle;
    }

    public String getNroLinDet() {
        return nroLinDet;
    }

    public void setNroLinDet(String nroLinDet) {
        this.nroLinDet = nroLinDet;
    }

    public String getTipoDTE() {
        return tipoDTE;
    }

    public void setTipoDTE(String tipoDTE) {
        this.tipoDTE = tipoDTE;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getQtyItem() {
        return qtyItem;
    }

    public void setQtyItem(String qtyItem) {
        this.qtyItem = qtyItem;
    }

    public String getUnmdItem() {
        return unmdItem;
    }

    public void setUnmdItem(String unmdItem) {
        this.unmdItem = unmdItem;
    }

    public String getVlrCodigo() {
        return vlrCodigo;
    }

    public void setVlrCodigo(String vlrCodigo) {
        this.vlrCodigo = vlrCodigo;
    }

    public String getNmbItem() {
        return nmbItem;
    }

    public void setNmbItem(String nmbItem) {
        this.nmbItem = nmbItem;
    }

    public String getPrcItem() {
        return prcItem;
    }

    public void setPrcItem(String prcItem) {
        this.prcItem = prcItem;
    }

    public String getPrcItemSinIgv() {
        return prcItemSinIgv;
    }

    public void setPrcItemSinIgv(String prcItemSinIgv) {
        this.prcItemSinIgv = prcItemSinIgv;
    }

    public String getMontoItem() {
        return montoItem;
    }

    public void setMontoItem(String montoItem) {
        this.montoItem = montoItem;
    }

    public String getIndExe() {
        return indExe;
    }

    public void setIndExe(String indExe) {
        this.indExe = indExe;
    }

    public String getCodigoTipoIgv() {
        return codigoTipoIgv;
    }

    public void setCodigoTipoIgv(String codigoTipoIgv) {
        this.codigoTipoIgv = codigoTipoIgv;
    }

    public String getTasaIgv() {
        return tasaIgv;
    }

    public void setTasaIgv(String tasaIgv) {
        this.tasaIgv = tasaIgv;
    }

    public String getImpuestoIgv() {
        return impuestoIgv;
    }

    public void setImpuestoIgv(String impuestoIgv) {
        this.impuestoIgv = impuestoIgv;
    }

    public String getCodigoProductoSunat() {
        return codigoProductoSunat;
    }

    public void setCodigoProductoSunat(String codigoProductoSunat) {
        this.codigoProductoSunat = codigoProductoSunat;
    }
    
    
   
    
}
