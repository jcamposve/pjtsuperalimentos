/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.task;

import com.facturabachsa.proceso.ProcesoCabDet;
import com.facturabachsa.shared.ExceptionHandler;
import java.sql.SQLException;
import java.text.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alberto Fernandez
 */
//@Service
//@Transactional
@Service
public class Programacion {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Async
    @Scheduled(fixedRate = 3000)
    public void reportCurrentTime() {

        try {
            ProcesoCabDet.ProcesarFactura();
        } catch (SQLException | ParseException ex) {
            ExceptionHandler.handleException(ex, logger);
        }
    }
}
