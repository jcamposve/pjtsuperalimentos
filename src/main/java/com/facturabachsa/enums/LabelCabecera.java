/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.enums;

/**
 *
 * @author Alberto Fernandez
 */
public enum LabelCabecera {
    LlaveCabecera,
    CODI_EMPR,
    forma_de_pago,
    TipoDTE,
    Serie,
    Correlativo,
    FchEmis,
    HoraEmision,
    Moneda,
    RUTEmis,
    TipoRucEmis,
    RznSocEmis,
    ComuEmis,
    DirEmis,
    CodigoLocalAnexo,
    TipoRutReceptor,
    RUTRecep,
    RznSocRecep,
    DirRecep,
    Sustento,
    TipoNotaCredito,
    MntNeto,
    MntExe,
    MntTotGrat,
    MntTotAnticipo,
    MntTotal,
    TipoOperacion,
    FechVencFact,
    CodigoImpuesto,
    MontoImpuesto,
    TasaImpuesto
}
