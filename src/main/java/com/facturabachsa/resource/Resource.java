/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ResourceUtils;

/**
 *
 * @author root
 */
public class Resource {
    private static final String DOCCABORIGEN = "doccaborigen.rtf";
    
    private static Resource instance=null;
    
    public InputStream getQueryDocCabOrigenTxt() throws URISyntaxException, IOException{           
        InputStream archivo=new ClassPathResource(DOCCABORIGEN).getInputStream();
        return archivo;        
    }
        
    
    protected Resource() {      
   }
    
    public static Resource getInstance() {
      if(instance == null){
         instance = new Resource();
      }
      return instance;
   }
    
}

