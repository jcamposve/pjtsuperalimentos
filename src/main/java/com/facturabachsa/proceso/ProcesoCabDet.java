/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturabachsa.proceso;

import com.facturabachsa.bean.ParamFactura;
import com.facturabachsa.dao.DaoDocumentoCabecera;
import com.facturabachsa.dao.DaoParamIni;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 *
 * @author Alberto Fernandez
 */
public class ProcesoCabDet { 
    
    public static int ProcesarFactura() throws SQLException, ParseException
    {
        int result = 0;
        
        try 
        {
            DaoParamIni daoParamIni = new DaoParamIni();
            String paramFechaIni;
            String paramFechaFin;
            String strFechaFinal;
            String strFechaInicial;
            int cantDias;
            
            Date dtFechaInicial;
            Date dtParamFechaDbIni;

            ParamFactura paramfac = new ParamFactura();
            daoParamIni.GetParamFacturacion(paramfac);
            paramFechaIni = paramfac.getFechaIni();
            paramFechaFin = paramfac.getFechaFin();
            cantDias = Integer.parseInt(paramfac.getCantDias());

            Calendar calendarIni =Calendar.getInstance();
            Calendar calendarFin =Calendar.getInstance();
            SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
            calendarIni.add(Calendar.DATE, -cantDias);
            calendarFin.add(Calendar.DATE,-0);
            strFechaInicial = sm.format(calendarIni.getTime());
            // = "2018-12-18";

            dtFechaInicial = sm.parse(strFechaInicial);
            dtParamFechaDbIni = sm.parse(paramFechaIni);

            if (dtFechaInicial.before(dtParamFechaDbIni))
            {
                strFechaInicial = paramFechaIni;
            }
            
            if(null == paramFechaFin || paramFechaFin.isEmpty())
            {
                strFechaFinal = sm.format(calendarFin.getTime()); 
                //strFechaFinal = "2018-12-18";
            }
            else
            {
                strFechaFinal = paramFechaFin;
            }
            
            DaoDocumentoCabecera ddc = new DaoDocumentoCabecera();

            result = ddc.InsertarDocumento(strFechaInicial, strFechaFinal);
            
        }
        catch(SQLException ex)
        {
            throw ex;
        }
        
        
        return result;     
    }
        

}
